# README #

hello! Welcome to FramwOrg!
this is a quick readme about this project

### How do I get set up? ###

* get python
* get pyramid
* get sqlalchemy
* get framworg
* change the conf
* upload!

### Contribution guidelines ###

* Writing tests
* Code review
* add features

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contac

### what is framwOrg ? ###

framwOrg is in the middle between a framework and a cmd, it designed to help any organisation to have a foot in the internet and manage their stuff in a healthy way

### open source! ###

The MIT License (MIT)

Copyright (c) 2015 Baptiste Mouterde
Copyright (c) 2015 Omar Latreche

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
