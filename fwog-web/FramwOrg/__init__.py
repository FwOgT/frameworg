from pyramid.config import Configurator
from sqlalchemy import engine_from_config
from pyramid.session import SignedCookieSessionFactory
from pyramid.config import Configurator

from .models import (
    DBSession,
    Base,
    )


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    my_session_factory = SignedCookieSessionFactory('afazfqsf6qsf54art135H12I9y')
    config = Configurator(settings=settings)
    config.set_session_factory(my_session_factory)
    
    config.include('pyramid_mako')
    config.add_static_view('static', 'static', cache_max_age=3600)

    config.add_route('home', '/')

    config.add_route('man_user', '/man_user')
    config.add_route('add_user', '/add_user')
    config.add_route('sup_user', '/sup_user')
    config.add_route('upd_user', '/upd_user')
    config.add_route('user', '/user/{login}')
    config.add_route('users', '/users/')
    config.add_route('login', '/login')
    config.add_route('logout', '/logout')

    config.scan()

    return config.make_wsgi_app()
