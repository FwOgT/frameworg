from sqlalchemy import Column, Integer, ForeignKey, String
import transaction
from FramwOrg import DBSession
from FramwOrg.model.Entity import Entity


class Group(Entity):
    __tablename__ = "Group"
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, ForeignKey('Entity.id'), primary_key=True)
    parent_id = Column(Integer, ForeignKey("Group.id"))
    description = Column(String)


    __mapper_args__ = {
        'polymorphic_identity': 'Group'
    }

    #def add_user(self, user):


    def save(self):
        session = DBSession()
        session.add(self)
        try:
            transaction.commit()
        except:
            session.rollback()
            raise

    def delete(self):
        session = DBSession()
        session.delete(self)
        try:
            transaction.commit()
        except:
            session.rollback()
            raise