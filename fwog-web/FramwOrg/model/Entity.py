from sqlalchemy.orm import relationship
import transaction
from FramwOrg.models import Base, DBSession
from sqlalchemy import Column, Integer, String, ForeignKey, MetaData, Table


class EntityToEntity(Base):
    __tablename__ = "EntityToEntity"
    left_entity_id = Column(Integer, ForeignKey("Entity.id"), primary_key=True)
    right_entity_id = Column(Integer, ForeignKey("Entity.id"), primary_key=True)


class Entity(Base):
    __tablename__ = "Entity"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    parent = relationship("Entity",
                          secondary=EntityToEntity.__table__,
                          primaryjoin='Entity.id==EntityToEntity.left_entity_id',
                          secondaryjoin='Entity.id==EntityToEntity.right_entity_id',
                          post_update=True
    )

    type = Column(String(50))
    __mapper_args__ = {
        'polymorphic_identity': 'Entity',
        'polymorphic_on': type
    }

    def delete(self):
        session = DBSession()
        session.delete(self)
        try:
            transaction.commit()
        except:
            session.rollback()
            raise
