from _sha256 import sha256
from sqlalchemy.orm import relationship, backref
from FramwOrg import DBSession
from FramwOrg.model.Entity import Entity
from FramwOrg.config import conf
import logging
import transaction

log = logging.getLogger(__name__)

__author__ = 'kolo'
from sqlalchemy import Column, String, Integer, Date, Boolean, ForeignKey


def get_user_by_login(login):
    session = DBSession()
    result = session.query(User).filter(User.login == login)
    return result.first()


def get_all_user():
    session = DBSession()
    result = session.query(User, User.login)
    return result.all()


class User(Entity):
    __tablename__ = "User"
    id = Column(Integer, ForeignKey('Entity.id'), primary_key=True)
    first_name = Column(String)
    addresses = relationship("Address", single_parent=True, order_by='Address.date_in',
                             backref=backref("User", single_parent=True, cascade="save-update, merge, "
                                                             "delete, delete-orphan"), post_update=True)
    mail = Column(String, unique=True)
    password = Column(String)
    birthday = Column(Date)
    civility = Column(String)
    is_admin = Column(Boolean)
    login = Column(String)
    groups = relationship("Group", single_parent=True, backref=backref("Users", cascade="save-update, merge, "
                                                                    "delete, delete-orphan"),
                          post_update=True)

    __mapper_args__ = {
        'polymorphic_identity': 'User'
    }

    def auth(self, password):
        if password == conf.admin_password and self.login == conf.admin_login:
            return True
        else:
            auth_user = get_user_by_login(self.login)
            if auth_user is not None:
                hash_object = sha256(password.encode('utf-8') + 'ceci est un sel random'.encode('utf-8'))
                if hash_object.hexdigest() == auth_user.password:
                    return True
            return False

    def __init__(self, login):
        self.login = login
        self.first_name = "login"

    def set_value(self, first_name, last_name, civility, mail, password, is_admin=False):
        self.first_name = first_name
        self.name = last_name
        self.civility = civility
        self.mail = mail
        hash_object = sha256(password.encode('utf-8') + 'ceci est un sel random'.encode('utf-8'))
        self.password = hash_object.hexdigest()
        self.is_admin = is_admin

    def save(self):
        session = DBSession()
        session.add(self)

        try:
            transaction.commit()
            session.flush()
        except:
            session.rollback()
            raise


