from FramwOrg.models import Base
from sqlalchemy import Integer, Column, String, Date, ForeignKey


class Address(Base):
    __tablename__ = "Address"
    id = Column(Integer, primary_key=True)
    user_id=Column(Integer, ForeignKey('User.id'))
    zip = Column(Integer)
    street = Column(String)
    door = Column(Integer)
    date_in = Column(Date)