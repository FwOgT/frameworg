from FramwOrg.config.conf import *

__author__ = 'Omar'


class Context:
    org_name = ""
    site_name = ""
    site_head = ""
    site_foot = ""
    site_body = ""
    logo = 'FramwOrg:static/images/logo.PNG'

    user_log = ""

    admin_log = ""
    admin_pas = ""

    param = {}
    """
    This is the context that allow information
    that need to passe by page to other
    """

    def __init__(self):
        """
        Contstructeur
        :return:
        """
        self.site_name = siteWeb_name
        self.org_name = organisation_name
        self.logo = logo_link
        self.admin_log = admin_login
        self.admin_pas = admin_password
        self.site_head = head_message
        self.site_foot = foot_message
        self.site_body = body_message
        pass

    def set_user(self, login, password):
        self.user_log = login
        self.user_pas = password
