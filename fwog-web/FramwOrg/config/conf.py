__author__ = 'kolo'

admin_login = "Baptiste"
admin_password ="fwog"

organisation_name = "Votre Organisation"
logo_link = 'FramwOrg:static/images/logo.PNG'
siteWeb_name = "Nom de votre Site"
head_message = "Bienvenue chez FwOg"
foot_message = "FramwOrg, une librairie faire pour les organisations avec <3";

body_message = "FwOg Démonstration : ce site a pour but d'illustrer le packaqge FramwOrg\n \n" \
               "L’idée novatrice derrière FramewOrg est de proposer un outil flexible et évolutif \n" \
               "centralisant les besoins des organisations dans une seule application.\n\n " \
               "Fonctionnalitées de la librairie FwOg v0.1 : \n\n" \
               "Module utilisateur : \n" \
               "   - l’authentification, \n" \
               "   - l’ajout  \n" \
               "   - la modification \n" \
               "   - la suppression d’un utilisateur  \n" \
               "Module groupe : \n" \
               "   - l’ajout \n" \
               "   - la modification\n" \
               "   - la suppression \n" \
               "   - l’envoi d’un mail à un groupe\n" \
               "\n\n" \
               "Version livré le 12 févier 2015 \n" \
               "Réalisée par Baptiste Mouterde et Omar Latreche\n" \
               "Licence MIT \n" \
               "Enjoy"