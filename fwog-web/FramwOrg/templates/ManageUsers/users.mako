<%inherit file="../base.mako"/>

<%block name="title">
    User Description
</%block>
<%block name="h1">
    listes des utilisateurs
</%block>
<article class="lineflexbox">
    <div class="columnflexbox">
        %for user in cont.param["users"] :
            <a href="${request.route_url("user",login=user.login)}"> ${user.login}</a>
        %endfor
    </div>
</article>