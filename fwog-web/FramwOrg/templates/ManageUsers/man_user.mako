<%inherit file="../base.mako"/>
<%block name="title">
    Administration
</%block>

<%block name="h1">
   Administration
</%block>
<article class="columnflexbox">
    <form method="get" class="columnflexbox baselineflex" action="${request.route_url('add_user')}">
       <button type="submit">Ajouter un utilisateur </button>
    </form>
        <form method="get" class="columnflexbox baselineflex" action="${request.route_url('sup_user')}">
       <button type="submit">Supprimer un utilisateur </button>
    </form>
    </form>
        <form method="get" class="columnflexbox baselineflex" action="${request.route_url('user',login='')}">
       <button type="submit">Consulter un utilisateur </button>
    </form>
</article>