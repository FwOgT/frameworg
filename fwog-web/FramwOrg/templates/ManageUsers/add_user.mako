<%inherit file="../base.mako"/>

<%block name="title">
    Ajouter un utilisateur
</%block>
<%block name="h1">
    Ajout d'un utilisateur
</%block>
<article class="columnflexbox">
    <form method="post" class="columnflexbox baselineflex" target="_self">
        <input name="login" placeholder="login" value= ""/>
        <input name="name" placeholder="nom" value= ""/>
        <input name="last_name" placeholder="prenom" value= ""/>
        <input name="mail" placeholder="mail" value= ""/>
        <input name="civility" placeholder="civilité" value= ""/>
        <input name="password" type="password" value="bobafette"/>
        <button>Ajouter cet utilisateur</button>
    </form>
</article>