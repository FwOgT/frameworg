<nav class="columnflexbox">
    <a class="flexoneauto" href="">
        Bienvenue, ${ request.session["user"].login if request.session.created !=0 and "user" in request.session  else "invité"}
    </a>
 % if "user" in request.session:
     <a class="flexoneauto" href="${request.route_url('logout')}">
         déconnection
     </a>
 % else:
    <a class="flexoneauto" href="${request.route_url('login')}">
        connection
    </a>
 %endif

 </nav>