<%inherit file="base.mako"/>

<%block name="title">
    ${cont.site_name}
</%block>

<%block name="h1">
    ${cont.site_head}
</%block>
    <article class="lineflexbox">
        <img src="${request.static_url(cont.logo)}" style="max-height:500px;" alt="logo"/>

    <div style="white-space:pre-wrap">
        ${cont.site_body}
    </div>
    </article>
