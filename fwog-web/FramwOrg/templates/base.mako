<html>
    <head>
       <link rel="stylesheet" href="${request.static_url('FramwOrg:static/main.css')}"/>
    </head>

    <title>
            <%block name="title"/>
    </title>
    <body class="columnflexbox">
         <header class="menu lineflexbox">

             <nav class="flexoneauto lineflexbox">
                 <a href="/">${cont.org_name}</a>
                 <a href="${request.route_url('man_user')}">Gestion des utilisateurs</a>
                 <a href="${request.route_url('login')}">Authentification</a>
                 <a href="${request.route_url('users')}">utilisateurs</a>

             </nav>

            <%include file='connection_state.mako'></%include>

         </header>
         <h1 class="title">
                 <%block name="h1"/>
         </h1>
         <section class="columnflexbox flexoneauto">
             ${self.body()}
         </section>
         <footer class="footer">
             ${cont.site_foot}
         </footer>
    </body>
</html>