from pyramid.httpexceptions import HTTPFound
from pyramid.security import remember
from pyramid.view import view_config
from FramwOrg.config.context import *

import logging
from FramwOrg.model.Entity import Entity
from FramwOrg.model.User import User, get_user_by_login, get_all_user

log = logging.getLogger(__name__)
cont = Context()


@view_config(route_name='login', renderer='templates/login.mako')
def login_view(request):
    next_page = request.params.get('next') or request.route_url('home')
    if "password" in request.POST and "login" in request.POST:
        user = User(request.POST["login"])
        user.login = request.POST["login"]
        if user.auth(request.POST["password"]):
            headers = remember(request, user.login)
            request.session["user"] = user
            return HTTPFound(location=next_page, headers=headers)
        else:
            cont.param["error"] = True
            return {'cont': cont}
    cont.param["error"] = False
    return {'cont': cont}


@view_config(route_name='logout')
def logout_view(request):
    request.session.invalidate()
    return HTTPFound(location=request.route_url('home'))


# block User
@view_config(route_name='man_user', renderer='templates/ManageUsers/man_user.mako')
def man_user_view(request):
    return {'cont': cont}


@view_config(route_name='sup_user', renderer='templates/ManageUsers/sup_user.mako')
def sup_user_view(request):
    if request.POST and "login" in request.POST:
        user = get_user_by_login(request.POST["login"])
#        user.delete()
        return HTTPFound()
    return {'cont': cont}


@view_config(route_name='add_user', renderer='templates/ManageUsers/add_user.mako')
def add_user_view(request):
    if request.POST and "login" in request.POST \
            and "name" in request.POST \
            and "last_name" in request.POST \
            and "mail" in request.POST \
            and "civility" in request.POST \
            and "password" in request.POST:
        u = User(request.POST["login"])
        u.set_value(request.POST["name"], request.POST["last_name"], request.POST["civility"], request.POST["mail"],
                    request.POST["password"])

        next_page = request.route_url('user', login=u.login)
        u.save()
        return HTTPFound(location=next_page)
    else:
        cont.param["login"] = ""
        return {'cont': cont}


@view_config(route_name='user', renderer='templates/ManageUsers/user.mako')
def user_view(request, login=''):
    user=get_user_by_login(request.matchdict["login"])
    cont.param["user"]=user
    return {'cont': cont}


@view_config(route_name='users', renderer='templates/ManageUsers/users.mako')
def users_view(request):
    users = get_all_user()
    cont.param["users"] = users
    return {'cont': cont}


# Fin Block User




@view_config(route_name='home', renderer='templates/home.mako')
def my_view(request):
    return {'cont': cont}


